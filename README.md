<h1 align="center">:file_cabinet: Service Vtex</h1>

## :memo: Descripción

Event Example esta siendo desarrollado siguiendo las lecciones del Learning Path de Vtex. El bloque cursado para realizar la app es: <b>Service (block 7)</b>. Debe ser levantada junto a  Vtex Course Event Example para un correcto funcionamiento.

## :wrench: Tecnologías utilizadas

- <b>Vtex CLI</b>
  <b>React</b>
  <b>TypeScript</b>
  <b>GraphQL</b>


## :rocket: Levantar el proyecto

Para rodar o repositório é necessário clonar o mesmo, dar o seguinte comando para iniciar o projeto:

```
<git clone https://gitlab.com/Alfredo_Supanta/vtex-course-event-example>
<vtex login xxxxxx>
<vtex link>
```

## :handshake: Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/AlfredoMarcelo">
        <img src="https://avatars.githubusercontent.com/u/82328683?s=96&v=4" width="100px;" alt="Foto de Alfredo en GitHub"/><br>
        <sub>
          <b>Alfredo Supanta</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
